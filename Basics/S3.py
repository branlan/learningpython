#!/usr/bin/python3
# Learning Python, March 15 2021
# Author - Bran Lan

# Global variables

animals = ""

# Defining a new function

def numAnimals():
	if int(animals) >= 4: 
		print("That's a lot of cuddles!")

	elif animals < int(0):
		print("You killed them!!!!!!!!!!!!!!")

	else:
		print("You need some more fur babies!")

while True:
	try:
		print("How many animals are in your house?")
		animals = int(input())
		break;
	except ValueError:
		print("Please enter a number...")
		animals = (input)

numAnimals()
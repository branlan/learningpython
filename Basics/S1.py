#!/usr/bin/python3
# Learning Python, March 6-7 2021
# Author - Bran Lan

import time

# Get name
friend = input("What is your name? ")
print("Hello", friend)
print("Your name is", len(friend), "characters long, that's neat")

# Get age
print("How old are you?")
age = input()

# Validates that age is an int, if not asks again
while True:
	try:
		val = int(age)
		# Print age and exits
		print("Cool!", friend, "is", val, "and will be", int(age) + 1, "in 1 year!")
		break;
	except ValueError:
		# Try again
		print("No... Please enter a number")
		age = input()

# End
time.sleep(5)
print("See, no one cares.")
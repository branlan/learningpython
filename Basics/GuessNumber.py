#!/usr/bin/python3
# Learning Python, September 8 2021
# Author - Bran Lan
# Guess the number

import random

print("hello, what is your name?")
name = input()

print("Hello there " + name + ", guess the number I'm thinking of...")

secretNumber = random.randint(1, 20)

for guessesTaken in range(1, 7):
    print("Take a guess!")
    guess = int(input())

    if guess < secretNumber:
        print("Your guess is too low!")
    elif guess > secretNumber:
        print("Your guess is too high!")
    else:
        break

if guess == secretNumber:
    print("Good job, you got it " + name + "!!")
else:
        print("Nope, the number I was thinking was " + str(secretNumber) + ".")

print("You took " + str(guessesTaken) + " guesses!")

# Goodbye...

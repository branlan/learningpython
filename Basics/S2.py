#!/usr/bin/python3
# Learning Python, March 10 2021
# Author - Bran Lan

# flow

print("Please enter a name")
name = input()

print("And your age?")
age = input()

# Validates that age is an int, if not asks again

while True:
	try:
		val = int(age)
		# age is number and exits
		break;
	except ValueError:
		# Try again
		print("No... Please enter a number")
		age = input()

spam = 0
age = int(age)

# while blank
while name == "":
	print("Please enter your name...")
	name = input()

if name == "Bran":
	print("Hello creator.")
	spam = 5

# elif statements only use the first true statement

elif age <= 12:
	print("Go home kid.")

elif age >= 2000:
	print("Unlike you, Alice isn't immortal...")

elif age >= 100:
	print("Alright, granny.")

else: 
	print("Oh my...")

# while & continue if spam == 0

while spam < 5:
	spam = spam + 1
	if spam == 0:
		continue
	print("spam is", str(spam))

# for runs i times

print("Your name is ")
for i in range(5):
	print(name + str(i))

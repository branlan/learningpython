#!/usr/bin/python3
# Learning Python, September 8 2021
# Author - Bran Lan

# lists

spam = ["cat","bat","rat","dog"]
# spam[1] = bat

spam2 = [["cat","bat"][10,20,30]]
# spam2[0][0] = cat | spam2[1][0] = 10
# spam2[-1][0] = bat
# spam2[0:1] = cat, bat,

del spam2[1] # Delete bat from spam2

len([1,2,3]) # = 3 | items in lists

